import { Injectable } from '@angular/core';
import { UserModel } from './user-model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  userList: Array<UserModel> = [];
  constructor() { }

  getAllItems()
  {
    this.userList = JSON.parse(localStorage.getItem("users"));
    if (this.userList == null) this.userList = [];;

    return this.userList;
  }
  
  getItem(i)
   {
    return this.userList[i];
   }
   
   addItem(item)
   {
  this.userList.push(item);
   localStorage.setItem('users',JSON.stringify(this.userList));  }
   
 deleteItem(i) 
 {
    this.userList.splice(i,1);
 
     localStorage.setItem('users', JSON.stringify(this.userList));
 }
  
  updateItem(i, newItem)
  {
    const userList = JSON.parse(localStorage.getItem("users")) || [];
    userList[i] = newItem;
   localStorage.setItem('users', JSON.stringify(userList));

  }
  login(Item)
  {
    const userList = JSON.parse(localStorage.getItem("users")) || [];
    const result = userList.find(x => x.email == Item.email);
    if (result.password == Item.password) {
      return true;
    }
    else {
      return false;
    }
  }
}
