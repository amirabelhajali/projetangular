import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserModel } from '../user-model';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { AuthServiceService } from '../auth-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  userList: Array<UserModel> = [];
  constructor(private _userService:UserServiceService,private router: Router, private _authService:AuthServiceService) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
    this.userList = this._userService.getAllItems();
  }

 
    loginUser()
    {
      
     if (this.userForm.invalid) {
      return;
    }
    if(this._userService.login(this.userForm.value) )
    {
      this._authService.login();

      this.router.navigateByUrl('/dashboard');
    }
    else
    this.userForm.patchValue({email: '', password: ''});
  }


}
