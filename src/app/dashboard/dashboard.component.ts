import { Component, OnInit } from '@angular/core';
import { UserModel } from '../user-model';
import { UserServiceService } from '../user-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userList: Array<UserModel> = [];
  headElements = ['Username', 'Password', 'Action'];
  userForm: FormGroup;
  user:UserModel;
  index= null;
  submitted = false;


  constructor(private _userService:UserServiceService) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
    this.userList = this._userService.getAllItems();
  }
  deleteUser(i)
  {
    this._userService.deleteItem(i);
    this._userService.getAllItems(); 
   }
   updateUser(i)
   {
  //  sharedMessage = this.index.asObservable();
  //  this.sharedMessage.subscribe(message => this.index = i)
    this.index=i;
   }
}
