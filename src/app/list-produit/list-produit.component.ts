import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Produit } from '../produit/produit.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProduitService } from '../produit/produit.service';

@Component({
  selector: 'app-list-produit',
  templateUrl: './list-produit.component.html',
  styleUrls: ['./list-produit.component.css']
})
export class ListProduitComponent implements OnInit {
  @Output() indexChange = new EventEmitter();
  produitList: Array<Produit> = [];
  headElements = ['Libelle', 'Description','date Expiration', 'Action'];
  produitForm: FormGroup;
  peoduit:Produit;
//  index= null;
  submitted = false;

  constructor(private _produitService:ProduitService) { }

  ngOnInit(): void {
    this.produitForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
    this.produitList = this._produitService.getAllProduct();
  }
  deleteProduit(i)
  {
    this._produitService.deleteProduct(i);
    this._produitService.getAllProduct(); 
   }
   updateProduit(i)
   {
  //  sharedMessage = this.index.asObservable();
  //  this.sharedMessage.subscribe(message => this.index = i)
   this.indexChange.emit(i);
   // this.index=i;
   }
}
