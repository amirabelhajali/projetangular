import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { UserModel } from '../user-model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  index= null;
  user:UserModel;
  userForm: FormGroup;
  submitted = false;
  constructor(private _userService:UserServiceService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
    });
    this.index=this.route.snapshot.paramMap.get("i");
    this.user=this._userService.getItem(this.index);
    this.userForm.patchValue(this.user);
   
  }
  get formControls() {
    return this.userForm.controls;
  }

  saveUpdate()
  {
   this.submitted = true;
   if(this.userForm.invalid) 
   {return;}
   this._userService.updateItem(this.index, this.userForm.value);
   this.index = null;
   this.submitted = false;
   this.userForm.patchValue({email: '', password: ''});
   this._userService.getAllItems(); 
  }
}
