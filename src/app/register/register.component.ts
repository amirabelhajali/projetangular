import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserModel } from '../user-model';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userForm: FormGroup;
  userList: Array<UserModel> = [];
  submitted = false;

  constructor(private _userService:UserServiceService) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
  }

  get formControls() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if(this.userForm.invalid) 
    {return;}
     
  this._userService.addItem(this.userForm.value);
  this.userForm.patchValue({email: '', password: ''});
  this.submitted = false;
}
   redirect()
   {}

}
