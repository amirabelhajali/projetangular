import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isLoggedIn : Observable<boolean>;
  constructor(public authService : AuthServiceService,private router: Router ) { 
    this.isLoggedIn = authService.isLoggedIn();
  }

  ngOnInit(): void {
  }
  logout()
  {
    this.authService.logout();
    this.router.navigateByUrl('/login');

  }

}
