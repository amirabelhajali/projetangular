import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFountComponent } from './not-fount/not-fount.component';
import { UpdateComponent } from './update/update.component';
import { ProduitComponent } from './produit/produit.component';
import { ListProduitComponent } from './list-produit/list-produit.component';



const routes: Routes = [
  {
    path:'dashboard',
    component: DashboardComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  
  {
    path:'update/:i',
    component: UpdateComponent
  },
  {
    path:'register',
    component: RegisterComponent
  },
  {
     path: '',
     redirectTo: 'login',
     pathMatch: 'full' 
  },
  {
    path:'addProduit',
    component: ProduitComponent
  },
  {
    path:'listProduit',
    component: ListProduitComponent
  },
   {
    path:'**',
    component: NotFountComponent
  }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
