import { Component } from '@angular/core';
import { UserModel } from './user-model';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from './user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  constructor(private router: Router) { }

  ngOnInit() {
   
  }
 
  
}
