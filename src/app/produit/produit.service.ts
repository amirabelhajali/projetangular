import { Injectable } from '@angular/core';
import { Produit } from './produit.model';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  produitList: Array<Produit> = [];

  constructor() { }

  getAllProduct()
  {
    this.produitList = JSON.parse(localStorage.getItem("produits"));
    if (this.produitList == null) this.produitList = [];

    return this.produitList;
  }
  
  getProduct(i)
   {
    return this.produitList[i];
   }
   
   addproduct(item)
   {
  this.produitList.push(item);
   localStorage.setItem('produits',JSON.stringify(this.produitList));  }
   
 deleteProduct(i) 
 {
    this.produitList.splice(i,1);
 
     localStorage.setItem('produits', JSON.stringify(this.produitList));
 }
  
  updateProduct(i, newItem)
  {
    const produitList = JSON.parse(localStorage.getItem("produits")) || [];
    produitList[i] = newItem;
   localStorage.setItem('produits', JSON.stringify(produitList));

  }


}
