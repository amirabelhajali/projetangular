export class Produit {
    constructor(
        public libelle: string,
        public description : string,
        public date: string
    ){}
}
