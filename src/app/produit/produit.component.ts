import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Produit } from './produit.model';
import { ProduitService } from './produit.service';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {
  produitForm: FormGroup;
  produitList: Array<Produit> = [];
  produit: Produit;
  submitted = false;
  index=null;
  constructor(private _produitService:ProduitService) { }

  ngOnInit(): void {
    this.produitForm = new FormGroup({
      libelle: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      date: new FormControl('', [Validators.required])
    })
  }
  get formControls() {
    return this.produitForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if(this.produitForm.invalid) 
    {return;}
     
  this._produitService.addproduct(this.produitForm.value);
  console.log(this.produitForm.value);
  this.produitForm.patchValue({libelle: '', description: '', date:''});
  this.submitted = false;
  this.index=null;
}

displayIndex(i)
{
  this.index=i
  this.produit=this._produitService.getProduct(this.index);
  this.produitForm.patchValue(this.produit);
//console.log(index);
}
saveUpdate()
{
 this.submitted = true;
 if(this.produitForm.invalid) 
 {return;}
 this._produitService.updateProduct(this.index, this.produitForm.value);
 this.index = null;
 this.submitted = false;
 this.produitForm.patchValue({libelle: '', description: ''});
 this._produitService.getAllProduct(); 
}
}
